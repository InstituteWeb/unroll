<?php

/*  | This extension is made for TYPO3 CMS and is licensed under GNU General Public License.
 *  | See LICENSE.txt shipped with this extension.
 *  |
 *  | (c) 2016-2017 Markus Klein, Reelworx GmbH https://reelworx.at/
 *  |     2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */

// @codingStandardsIgnoreStart
$EM_CONF[$_EXTKEY] = [
    'title' => 'Enhanced TYPO3 save buttons',
    'description' => 'Adds options to TYPO3\'s save buttons, to unroll them from dropdown, just display icons and changing the order.',
    'category' => 'backend',
    'version' => '2.0.2',
    'state' => 'stable',
    'uploadfolder' => false,
    'author' => 'Markus Klein, Armin Vieweg',
    'author_email' => 'support@reelworx.at,armin@v.ieweg.de',
    'author_company' => 'Reelworx GmbH, Institute Web',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.2-8.7.99',
            'setup' => '7.6.0-8.7.99',
        ],
        'conflicts' => [
            'rx_unrollsavebuttons' => '1.0.0-1.99.99',
        ],
        'suggests' => [],
    ],
    'createDirs' => null,
    'clearcacheonload' => false,
];
// @codingStandardsIgnoreEnd
